from django.core.management.base import BaseCommand, CommandError
from shortener.models import Surl

class Command(BaseCommand):
    help = 'refreshes all Surl shortcodes'

    def add_arguments(self, parser):
         parser.add_argument('items', type=int)

    def handle(self, *args, **options):
        print(options)
        return Surl.objects.refresh_shortcodes(items=options['items'])
 
