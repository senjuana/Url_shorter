from django.db import models
#utilidades
from .utils import create_shortcode

# Create your models here.

class SurlManager(models.Manager):
    def all(self,*args,**kwargs):
        qs_main = super(SurlManager,self).all(*args,**kwargs)
        qs = qs_main.filter(active=True)
        return qs

    def refresh_shortcodes(self,items=100):
        qs = Surl.objects.filter(id__gte=1)
        if items is not None and isinstance(items, int):
            qs = qs.order_by('-id')[:items]
        new_codes = 0
        for  q in qs:
            q.shortcode = create_shortcode(q)
            print(q.shortcode)
            q.save()
            new_codes += 1
        return "New codes made: {i}".format(i=new_codes)

class Surl(models.Model):
    url         = models.URLField(max_length=220)
    shortcode   = models.CharField(max_length=15,unique=True,blank=True)
    updated     = models.DateTimeField(auto_now=True)
    timestamp   = models.DateTimeField(auto_now_add=True)
    active      = models.BooleanField(default=True)

    objects = SurlManager()
    #some_random = SurlManager()

    #override de el metodo save al guardar cada objeto
    def save(self,*args,**kwargs):
        if self.shortcode is None or self.shortcode == "":
            self.shortcode = create_shortcode(self)
        super(Surl,self).save(*args,**kwargs)

    def __str__(self):
        return str(self.url)
