from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.views import View

from .models import Surl
# Create your views here.
#FBV
# def S_redirect_view(request,shortcode=None,*args,**kwargs):
#     obj = get_object_or_404(Surl,shortcode=shortcode)
#     return HttpResponseRedirect(obj.url)



class SCBview(View):
    def get(self,request,shortcode=None,*args,**kwargs):
        obj = get_object_or_404(Surl,shortcode=shortcode)
        return HttpResponseRedirect(obj.url)
