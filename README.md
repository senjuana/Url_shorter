# About
Este es un projecto de una web acortadora de Urls, como  bitly
creada desde cero en python.

[![build status](https://gitlab.com/senjuana/Url_shorter/badges/master/build.svg)](https://gitlab.com/senjuana/Url_shorter/commits/master) 
[![coverage report](https://gitlab.com/senjuana/Url_shorter/badges/master/coverage.svg)](https://gitlab.com/senjuana/Url_shorter/commits/master)
## Requirements

Supported operating systems:

* Development environments: GNU/Linux, MacOS X.

Requirements:

* virtualenv
* python  >= 3.5
* django >= 1.10
* crisy-forms >= 1.6.1

## Getting the code

    $ git clone https://senjuana@gitlab.com/senjuana/Url_shorter.git
    $ cd Url_shorter

## Runing The code
Lo primero que tienes que hacer  una vez tienes el repositorio clonado es crear un entorno vitrual

    $ vitualenv ShorterV
    $ cd ShorterV

Despues de crear el entorno vitual toca activar este entorno

    $ source bin/activate

Una vez con el entorno activado procederemos a instalar los requerimientos necesarias para correr la app      
para hacer esto  nos movemos a la carpeta del repositorio.

    $ cd .. && cd Url_shorter  
    $ pip install -r requirements.txt

Despues de instalar los requerimientos de la app lo unico que tienes que hacer es correr el servidor

    $ python manage.py runserver

Una vez el servidor de prueba este corriendo debes de entrar en el puerto local en el cual suele correr django.

http://127.0.0.1:8000/

# Contributing
Si encuentras un bug en el codigo o un issue, por favor notificame de manera privada en
[mi cuenta personal de twitter](https://twitter.com/senjuana).

Este proyecto sigue [code of merit](https://github.com/rosarior/Code-of-Merit). En este repositorio, me importa el codigo,
no opiniones personales o sentimientos. Espero tratar con  adultos.

Antes de enviar un pedazo de codigo por favor verifica que lo que envias funciona, yo no soy la persona que resolvera tus problemas con tu codigo.
