from django.conf.urls import url
from django.contrib import admin
#imports views
from shortener import views
from .views import HomeView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',HomeView.as_view()),
    # url(r'^a/(?P<shortcode>[\w-]+)/$',views.S_redirect_view),
    url(r'^(?P<shortcode>[\w-]+)/$',views.SCBview.as_view()),
]
