from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.views import View
from shortener.forms import SubmitUrlForm

class HomeView(View):
    def get(self,request,*args,**kwargs):
        the_form = SubmitUrlForm()
        contex = {
            "title":"Shu.com",
            "form" :the_form
        }
        return render(request,"index.html",contex)

    def post(self,request,*args,**kwargs):
        form = SubmitUrlForm(request.POST)
        contex = {
            "title":"Shu.com",
            "form" :form
        }
        if form.is_valid():
            print(form.cleaned_data)
        return render(request, "index.html",contex)
